<?php

require('autoload.php');
use App\Model\Worker;
use App\Model\ContractNotification;
use App\Model\ProgressNotification;

$worker = new Worker();

$newContract = new ContractNotification();

$newProgress = new ProgressNotification();

// S'inscrire à l'Observable

$worker->subscribeToNotification($newContract);
$worker->subscribeToNotification($newContract);
$worker->subscribeToNotification($newContract);

$worker->subscribeToNotification($newProgress);

//Envoyer la notification

$worker->update();
?>