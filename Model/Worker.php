<?php

namespace App\Model;

use App\Model\ManagerInterface;

//observable
class Worker{

    public $subscribers = array();

    public function subscribeToNotification(ManagerInterface $subscriber){
        array_push($this->subscribers,$subscriber );
    }

    function update(){
        foreach ($this->subscribers as $observer) {
            $observer->alert();
            echo '<br>';
        }
    }
}