<?php

namespace App\Model;

use App\Model\ManagerInterface;

class ProgressNotification implements ManagerInterface
{
    public function alert(){
        echo 'The building construction is in progress';
    }
}
