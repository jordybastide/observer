<?php

namespace App\Model;

use App\Model\ManagerInterface;

class ContractNotification implements ManagerInterface
{
    public function alert()
    {
        echo 'New contract signed';
    }
}
